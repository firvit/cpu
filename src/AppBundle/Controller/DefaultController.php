<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('AppBundle:Default:index.html.twig');
    }

    /**
     * @Route("/cpu", name="cpu")
     */
    public function cpuAction()
    {

        $stat1 = $this->getDataCpu();
        sleep(2);
        $stat2 = $this->getDataCpu();

        $dif = array();
        foreach ($stat1 as $k => $value ) {
            $dif[$k] = $stat2[$k] - $stat1[$k];
        }
        $total = array_sum($dif);

        $cpu = 100 - round($dif[5] / $total * 100);

        return new JsonResponse(array(
            'value' => $cpu
        ));
    }

    private function getDataCpu() {
        $data = file('/proc/stat');
        return explode(' ', $data[0]);
    }


}
